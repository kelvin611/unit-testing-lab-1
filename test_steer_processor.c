#include "unity.h"
#include "steer_processor.h"

#include "Mocksteering.h"

void test_steer_processor__move_left(void) {
	 
	 extern uint32_t threshold;
	 TEST_ASSERT_EQUAL(50,threshold);
	 steer_left_Expect();
 	 steer_processor(60, 20);
 	 steer_left_Expect();
 	 steer_processor(80, 45);
}

void test_steer_processor__move_right(void) {
	  steer_right_Expect();
  	  steer_processor(20, 70);
  	  steer_right_Expect();
  	  steer_processor(5, 55);
}

void test_steer_processor__both_sensors_less_than_threshold(void) {
	  
	  steer_right_Expect();
  	  steer_processor(20, 30);

	  steer_left_Expect();
  	  steer_processor(30, 20);
  	  
}

void test_steer_processor__do_not_move(void) {
	  steer_processor(70, 70);
	  steer_processor(70, 60);
	  steer_processor(90, 60);
}

// Do not modify this test case
// Modify your implementation of steer_processor() to make it pass
void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}
